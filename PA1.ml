let rec digitsOfInt n =
  if n = 0 then [0] 
	else let rec _rest v = if (v <= 0) then [] else (_rest (v / 10)) @ [ v mod 10 ] in _rest n
;;

let digitOfInt v = 
match (let rec digit n = match n with
| 0 -> [0]
| _ -> (n mod 10) :: digit(n/10) in digit v) with
| h::[] -> [h]
| h::t -> t

let rec restorative n = match (n - 1) with
| -1 -> []
| _ -> restorative (n/10) @ [(n mod 10)]

let rec sumList xs =
  if xs = [] then 0 
	else ((fun x -> match x with h::t -> h | [] -> failwith "empty") xs)
	+ (sumList ((fun x -> match x with h::t -> t | [] -> failwith "empty") xs));;
  
let rec digitalRoot n =
  if n < 10 then n else digitalRoot (sumList (digitsOfInt n))
  
let rec additivePersistence n =
	let rec _run m k =
		if m < 10 then k else _run (sumList (digitsOfInt n)) (k+1)
	in _run n 0
;;

let rec listReverse xs =
	if (xs = []) then [] else
	listReverse((fun x -> match x with h::t->t | [] -> failwith "empty") xs) @ 
		(fun x -> match x with h::t->h | []-> failwith "empty") xs :: []
;;

let explode n =
	let rec _expl v i =
		if i < String.length v then 
			v.[i] :: _expl v (i+1)
		else
			[]
	in _expl n 0
;;

let palindrome w = (explode w) = (listReverse (explode w));;

			