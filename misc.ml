(* CSE 130: Programming Assignment 1
 * misc.ml
 *)

(* sumList : int list -> int 
 * (sumList l) is the sum of an int list's elements
 * e.g. (sumList [1;2;3;] = 6
 *) 

let rec sumList l = 
	match l with
	| [] -> 0
	| x::s -> x + sumList(s)
;;


(* digitsOfInt : int -> int list 
 * (digitsOfInt n) is the list of digits in the order they appear, works with
 * positive numbers. On the input of a negative number, returns an empty int list
 * e.g. (digitsOfInt (-1)) is []
 * 		  (digitsOfInt (9876)) is [9;8;7;6;]
 *)

let rec digitsOfInt n = 
	if (n = 0) then [0]
	else let rec _h x = if x <= 0 then [] else _h (x/10) @ [x mod 10] in _h n
;;

(* digits : int -> int list
 * (digits n) is the list of digits of n in the order in which they appear
 * in n
 * e.g. (digits 31243) is [3,1,2,4,3]
 *      (digits (-23422) is [2,3,4,2,2]
 *)
 
let digits n = digitsOfInt (abs n)


(* From http://mathworld.wolfram.com/AdditivePersistence.html
 * Consider the process of taking a number, adding its digits, 
 * then adding the digits of the number derived from it, etc., 
 * until the remaining number has only one digit. 
 * The number of additions required to obtain a single digit from a number n 
 * is called the additive persistence of n, and the digit obtained is called 
 * the digital root of n.
 * For example, the sequence obtained from the starting number 9876 is (9876, 30, 3), so 
 * 9876 has an additive persistence of 2 and a digital root of 3.
 *)


(* ***** PROVIDE COMMENT BLOCKS FOR THE FOLLOWING FUNCTIONS ***** *)

(* accum : (int * int) -> (int * int) *)
(* (accum x n) is the tuple consisting of the digitalRoot and the*)
(* 		additivePersistence.*)
(* e.g. (accum 9876 0) is (3, 2) *)
let rec accum x n=
	if (x < 10) then (x, n) else accum (sumList (digitsOfInt x)) (n + 1)
;;

(* additivePersistence : int -> int *)
(* (additivePersistence n) is the value of the additivePersistence *)
(* e.g. (additivePersistence 9876) is 2 *)
let additivePersistence n =
	(fun (x,y) -> y) (accum n 0);;

(* digitalRoot : int -> int *)
(* (digitalRoot n) is the summation of the digits of a positive *)
(* integer and is repeated with its result *)
(* until the final sum is less than 10.*)
(* e.g. (digitalRoot 9876) is 3 *)
let digitalRoot n = 
	(fun (x,y) -> x) (accum n 0);;

(* listReverse : 'a list -> 'a list *)
(* (listReverse l) is a string whose ordering is reversed as they appear in l*)
(* e.g. (listReverse [1;2;3;4;]) is [4;3;2;1;]*)
(* 	    (listReverse ["S";"n";"a";"p";"p";"y"]) is ["y";"p";"p";"a";"n";"S"]*)
let listReverse l = 
	let rec _lr = function
		| [] -> []
		| x::s -> _lr(s) @ [x] in _lr l;;

(* explode : string -> char list 
 * (explode s) is the list of characters in the string s in the order in 
 *   which they appear
 * e.g.  (explode "Hello") is ['H';'e';'l';'l';'o']
 *)
let explode s = 
  let rec _exp i = 
    if i >= String.length s then [] else (s.[i])::(_exp (i+1)) in
  _exp 0

(* palindrome : string -> bool *)
(* (palindrome w) is true if 'w' is read the same backward as it is forward*)
(* e.g. (palindrome bob) is true  *)
(* 			(palindrome ann) is false *)
let palindrome w =
	if((explode w) = (listReverse (explode w))) then true else false;;