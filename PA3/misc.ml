(* CSE 130: Programming Assignment 3
 * misc.ml
 *)

(* For this assignment, you may use the following library functions:

   List.map
   List.fold_left
   List.fold_right
   List.split
   List.combine
   List.length
   List.append
   List.rev

   See http://caml.inria.fr/pub/docs/manual-ocaml/libref/List.html for
   documentation.
*)



(* Do not change the skeleton code! The point of this assignment is to figure
 * out how the functions can be written this way (using fold). You may only
 * replace the   failwith "to be implemented"   part. *)



(*****************************************************************)
(******************* 1. Warm Up   ********************************)
(*****************************************************************)
(* sqsum : int list -> int 
 * (sqsum xs) is the sum of each int's square in the list
 * e.g. (sqsum [] is 0
 *      (sqsum [1;2;3;4] is 30
 *)
let sqsum xs = 
  let f a x = (x * x) + a in
  let base = 0 in
    List.fold_left f base xs

(* pipe : ('a -> 'a) list -> ('a -> 'a) 
 * (pipe fs y) is the value of y piped through list functions [f1,f2,...,fn]
 * 		successively until f1(y)->f2(f1(y))->...->fn(fn-1(...(f2(f1(y)))))
 * e.g. (pipe [] 3 is 3
 *      (pipe [(fun x -> x+x);(fun x -> x + 3)] 3 is 9
 *)
let pipe fs = 
  let f a x = fun y -> x (a(y)) in
  let base = fun y -> y in
    List.fold_left f base fs

(* sepConcat : string -> string list -> string 
 * (sepConcat sep sl) is the concatonated string separated by symbol 'sep'
 * e.g. (sepConcat ", " ["Hello";"world"] is "Hello, world"
 *)
let rec sepConcat sep sl = match sl with 
  | [] -> ""
  | h :: t -> 
      let f a x = a ^ sep ^ x in
      let base = h in
      let l = t in
        List.fold_left f base l

(* (a' -> string) -> 'a list -> string                                       *)
(* (stringOfList f l) is the printed representation of a function f acted on *)
(*   a set of list elements, l, as were it the output.                       *)
(* eg. stringOfList string_of_int [1;2;3;4;5;6] is "[1;2;3;4;5;6]"           *)
(*     stringOfList (fun x -> x) ["foo"]        is "[foo]"                   *)
let stringOfList f l = "[" ^ (sepConcat "; " (List.map f l)) ^ "]"

(*****************************************************************)
(******************* 2. Big Numbers ******************************)
(*****************************************************************)
(* 'a->int->a' list                                                          *)
(* (clone x n) is a type of value repeated n many times into a list          *)
(* eg. clone 1 10      is [1;1;1;1;1;1;1;1;1;1]                              *)
(*     clone "hello" 2 is ["hello", "hello"]                                 *)
(*     clone [1;2] 2   is [[1;2];[1;2]]                                      *)
let rec clone x n = 
  let rec helper acc n' =
  match n' with
	| _ when (n' < 0) -> []
  | 0 -> acc
  | _ -> helper (x::acc) (n'-1) in
  helper [] n;;

(* int list -> int list -> int list * int list                               *)
(* (padZero l1 l2) are two lists of the same length, padded with zeros if    *)
(*     it is required to statisfy the condition they are the same length     *)
(* eg. padZero [1][1;2;3;4] is ([0;0;0;1],[1;2;3;4])                         *)
(*     padZero [0;0;1][1]   is ([0;0;1],[0;0;1])                             *)
let rec padZero l1 l2 =
	if List.length l1 = List.length l2 then (l1, l2)
	else let helper s =
		if s < 0 then padZero ((clone 0 (abs s))@l1) l2
		else padZero l1 ((clone 0 (abs s))@l2)
	in helper (List.length l1 - List.length l2);;

(* int list -> int list                                                      *)
(* (removeZero l) is an integer list that has all insignificant valued       *)
(*   zeros removed from the list. With the exception of [0] which is []      *)
(* eg. removeZero [0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1] is [1]       *)
(*     removeZero [0] is []                                                  *)
(*     removeZero [0;1;0;0] is [1;0;0]                                       *)
let rec removeZero l = 
	match l with
	| []   -> []
	| h::t -> if h = 0 then removeZero t else l

(* int list -> int list -> int list 
 * (bigAdd l1 l2) is the operation for typical addition emulating standard
 *   addition across normal integers. 
 * eg. bigAdd [1]     [2]      is [3]
 *     bigAdd [1;2;3;][1;2;3;] is [2;4;6]
 *)
let bigAdd l1 l2 =
	let add (l1, l2) =
		let f a x = 
		match x with
		| (x',y') ->
  		match a with
  		| (c, s) when (c + x' + y' >= 10) -> 
  			    (1, (c + x' + y') mod 10::s)
  		| (c, s) -> (0, (c + x' + y')::s) in
		let base = (0,[]) in
		let args = List.rev (List.combine (0::l1) (0::l2)) in
		let (_, res) = List.fold_left f base args in
		res
	in
	removeZero (add (padZero l1 l2));;

(* int -> int list -> int list
 *  (mulByDigit i l) is the multiplication of i into l as though l were a
 *		standard multiplication across an integer.
 * eg. mulByDigit 9 [1;1;1] is [9;9;9] 
 *     mulByDigit 8 [9;9;9] is [7;8;8;2] 
 *)
let rec mulByDigit i l = 
  let rec helper i acc =
  match i with
    | _ when (i < 0) -> []
    | 0 -> acc
    | _ -> helper (i - 1) (bigAdd acc l)
  in helper i [0];;

(* int list -> int list -> int list                                          *)
(* (bigMul l1 l2) is list representation of the decimal values of l1 and l2  *)
(*    multiplied together without the datatype restriction of integers.      *)
(* eg. bigMul [9;9;9;9][9;9;9;9] is [9;9;9;8;0;0;0;1]                        *)
(*     bigMul [1;2;3][2]         is [2;4;6]                                  *)
(*     bigMul [2][1;2;3]         is [2;4;6]                                  *)
let bigMul l1 l2 = 
  let f a x =
		match a with
		| (x',y') -> ((x' - 1), bigAdd(mulByDigit x (l2@(clone 0 x'))) y') in
  let base = ((List.length l1)-1,[]) in
  let args = l1 in
  let (_, res) = List.fold_left f base args in
    res
;;