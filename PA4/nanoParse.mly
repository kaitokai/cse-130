%{
(* See this for a tutorial on ocamlyacc 
 * http://plus.kaist.ac.kr/~shoh/ocaml/ocamllex-ocamlyacc/ocamlyacc-tutorial/ *)
open Nano 

%}

%token <int> Num
%token TRUE FALSE
%token <string> Id
%token EOF

%start prog
%type <Nano.expr> prog

%%

prog :
| exp EOF { $1 }

exp :
| Num                           { Int $1 }
| Id														{ Var($1)}