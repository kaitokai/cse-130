{
  open Nano        (* nano.ml *)
  open NanoParse   (* nanoParse.ml from nanoParse.mly *)
}

rule token = parse
  |  eof         { EOF }
  | _           { raise (NanoMLParseError
                          ("Illegal Character '"^(Lexing.lexeme lexbuf)^"'")) }
  | ['A'-'Z' 'a'-'z']['A'-'Z' 'a'-'z' '0'-'9']* as str {Id(str)}
	| ['0' - '9']+ as num {Num (int_of_string num)}
	| "true"      {TRUE}
	| "false"     {FALSE}