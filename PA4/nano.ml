
type binary_operator = 
  | Plus
  | Minus
  | Mul
  | Div
  | Eq
  | Ne
  | Lt
  | Le
  | And
  | Or
  | Cons

type expr =   
  | Int of int
  | Bool of bool
  | Var of string
  | BinaryOp of expr * binary_operator * expr
  | Let of string * expr * expr
  | If of expr * expr * expr
  | Fun of string * expr
  | App of expr * expr
  | LetRec of string * expr * expr
  | Nil

exception NanoMLParseError of string
exception NanoMLEvalError of string
	
type value =  
  | VInt of int
  | VBool of bool
  | VClosure of env * string option * string * expr
  | VNil
  | VPair of value * value

and env = (string * value) list

let strBinOp : binary_operator -> string =
function
  | Plus -> "+"
  | Minus -> "-"
  | Mul -> "*"
  | Div -> "/"
  | Eq -> "="
  | Ne -> "<>"
  | Lt -> "<"
  | Le -> "<="
  | And -> "&&"
  | Or -> "||"
  | Cons -> "::"

let spr = Printf.sprintf

let indent k = String.make (2 * k) ' '

let rec strValue : value -> string =
function
  | VInt i -> spr "%d" i
  | VBool b -> spr "%b" b
  | VNil -> "[]"
  | VPair (v1, v2) -> spr "(%s::%s)" (strValue v1) (strValue v2) 
  | VClosure (env, fo, x, e) -> 
      let fs = match fo with
        | None   -> "Anonymous"
        | Some f -> spr "Named %s" f in
      spr "{%s,%s,%s,%s}" (strEnv env) fs x (strExpr 0 e)

and strEnv : env -> string =
fun env ->
  let xs = List.map (fun (x,v) -> spr "%s:%s" x (strValue v)) env in
  spr "[%s]" (String.concat ";" xs)

and strExpr : int -> expr -> string =
fun depth e -> match e with
  | Int i ->
      string_of_int i
  | Bool b ->
      string_of_bool b
  | Var x ->
      x
  | BinaryOp (e1, op, e2) ->
      spr "(%s %s %s)" (strExpr depth e1) (strBinOp op) (strExpr depth e2)
  | If (e1, e2, e3) ->
      spr "%s%s%s%s%s"
        (spr "if %s {\n"    (strExpr depth e1))
        (spr "%s%s\n"       (indent (succ depth)) (strExpr (succ depth) e2))
        (spr "%s} else {\n" (indent depth))
        (spr "%s%s\n"       (indent (succ depth)) (strExpr (succ depth) e3))
        (spr "%s}"          (indent depth))
  | Let (x, e1, e2) ->
      spr "%s%s%s%s"
        (spr "let %s =\n" x)
        (spr "%s%s\n" (indent (succ depth)) (strExpr (succ depth) e1))
        (spr "%sin\n" (indent depth))
        (spr "%s%s"   (indent (succ depth)) (strExpr (succ depth) e2))
  | App (e1, e2) ->
      spr "(%s %s)" (strExpr depth e1) (strExpr depth e2)
  | Fun (x, e) ->
      spr "function (%s) {\n%s%s\n%s}" x
        (indent (succ depth)) (strExpr (succ depth) e)
      (indent depth)
  | Nil -> "[]"
  | LetRec (x, e1, e2) ->
      spr "%s%s%s%s"
        (spr "let rec %s =\n" x)
        (spr "%s%s\n" (indent (succ depth)) (strExpr (succ depth) e1))
        (spr "%sin\n" (indent depth))
        (spr "%s%s"   (indent (succ depth)) (strExpr (succ depth) e2))

(*********************** Misc **********************************************)

let binop op e1 e2 = BinaryOp (e1, op, e2)
let plus           = binop Plus
let mul            = binop Mul
let div            = binop Div
let minus          = binop Minus
let eq             = binop Eq
let cons           = binop Cons

(*********************** Some helpers you might need ***********************)

let rec fold f acc args = 
  match args with
    | []   -> acc
    | h::t -> fold f (f (acc,h)) t

let listAssoc (k,l) = 
  fold (function
    | None, (k',v) -> if k = k' then Some v else None
    | acc, _       -> acc
  ) None l

(*********************** Your code starts here ****************************)

let lookup (x,evn) = failwith "to be written"

let rec eval (evn,e) = failwith "to be written"

