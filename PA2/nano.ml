open Printf

type binary_operator = 
  | Plus
  | Minus
  | Mul
  | Div
  | Eq
  | Ne
  | Lt
  | Le
  | And
  | Or

type expr =   
  | Int of int
  | Bool of bool
  | Var of string
  | BinaryOp of expr * binary_operator * expr
  | Let of string * expr * expr
  | If of expr * expr * expr
  | Fun of string * expr
  | App of expr * expr

let indent k = String.make (2*k) ' '

let strBinOp : binary_operator -> string =
function
  | Plus -> "+"
  | Minus -> "-"
  | Mul -> "*"
  | Div -> "/"
  | Eq -> "="
  | Ne -> "<>"
  | Lt -> "<"
  | Le -> "<="
  | And -> "&&"
  | Or -> "||"

let rec strExpr depth e =
  (indent depth) ^ 
  match e with
  | Int i -> string_of_int i
  | Bool b -> string_of_bool b
  | Var v -> v
  | BinaryOp(x,y,z)->
	 	sprintf("%s %s %s")(strExpr 0 x)(strBinOp y)(strExpr 0 z)
  | Let(x,y,z)->
	  sprintf("let %s =\n%s\n%sin\n%s") 
	    x (strExpr (depth +1) y) (indent depth) (strExpr (depth + 1) z)
  | If(x,y,z)->
	  sprintf("if (%s) {\n%s\n%s} else {\n%s\n%s}")
	    (strExpr 0 x) (strExpr (depth + 1) y) (indent depth) 
		(strExpr (depth + 1) z) (indent depth)
  | Fun(x,y)->
	  sprintf("function (%s) {\n%s\n%s}") 
	    x (strExpr (depth + 1) y) (indent depth)
  | App(x,y)->
	  sprintf("%s (%s)") (strExpr 0 x) (strExpr 0 y)
		
let printExpr e =
  let s = strExpr 0 e in
  let _ = Printf.printf "%s\n" s in
  String.length s

