let rec superset elts = 
    match elts with
    | []   -> [[]]
    | h::t -> superset(t) @ (List.map ((fun x -> x) superset(t)))
